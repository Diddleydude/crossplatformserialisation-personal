# Cross Platform Serialisation Wrapper

This is a small cross platform serialisation wrapper (plus ~~some~~ almost no testing), most of the core code is based on [Multiplayer Game Programming: Architecting Networked Games](https://www.oreilly.com/library/view/multiplayer-game-programming/9780134034355/) - by Josh Glazer.  This project is looking to extract out the core library allowing students in the third year Multi-Player Game Development module to use it in their own games having read the relevent book chapters. 

## For Students ##

Be sure to clone with: ```git clone --recurse-submodules https://smu_sc_gj@bitbucket.org/smu_sc_gj/crossplatformserialisation.git```

## Targets ##

### Libraries ###

**networking** - Removed in this implementation. 

**strings** - Error logging etc.

**serialisation** - Serialisation of basic and complex types, memory streams and bit streams. 

### Executable Targets ###

**game** - From the book, planning to use this in the future.

**Client** - Quick and dirty serialisation test with a simple ```Player``` object being serialised to a stream copied to another buffer and unserialised. 

**NetworkGame_test** - An automatically named target making use of GoogleTest (the Google Unit Test Framework).  This is included to allow students to get some experience of unit testing and popular tools. It also provides an excellent source of documentation as the code in the tests can be used to understand the various classes used in the examples.

## Known Issues ##
1. Testing is incomplete
	- Linux Testing - **DONE**
	- Windows Testing - **DONE**
		- Works on lecturer machine with much faffing, need to work out procedure for the rest of the lab machines.
		- Hopefully just a question of opening ports and restarting firewall. 
		<!-- Need to link install guide for this -->
2. Some tests won't work on the university network
	- ~~Connect -- tries to contact an external 'echo' server.~~
	- Works provided windows/linux local network services are running on the machine. 
		- [Linux](http://www.yolinux.com/TUTORIALS/LinuxTutorialNetworking.html#INET)
			- Some linux distributions no longer provide these servers, so we have to make our own. See [here](https://nmap.org/ncat/guide/ncat-simple-services.html) <!-- Glenn ... make a script for this -->
		- [Windows](https://teckangaroo.com/tcpip-services-how-to-enable-tcp-ip-services-on-windows-10/)
3. Some tests require manual input
	- Listen -- waits for a connection on port 54321
	- Echo tests for TCP/UDP
	- **CTest** integration will mark these as failed unless the info can be sent (does all the tests at once!). 


## Notes ##
<!--
** Questions for Students - remove before going live **
	* The client / server both struggle work out how much data to expect.
	* How could this be overcome?
-->